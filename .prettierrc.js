module.exports = {
  arrowParens: 'always',
  bracketSpacing: true,
  semi: false,
  // overrides: [
  //   {
  //     files: '*.mdx',
  //     options: {
  //       parser: 'markdown',
  //     },
  //   },
  // ],
  printWidth: 80,
  singleQuote: true,
  tabWidth: 2,
}
