import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'

export function getEntityFiles(entityDirectory) {
  return fs.readdirSync(entityDirectory)
}

export function getEntityDirectory(entity) {
  return path.join(process.cwd(), entity)
}

export function getEntityData(entityIdentifier, entityDirectory) {
  const entitySlug = entityIdentifier.replace(/\.md$/, '') // removes the file extension
  const filePath = path.join(entityDirectory, `${entitySlug}.md`)
  const fileContent = fs.readFileSync(filePath, 'utf-8')
  const { data, content } = matter(fileContent)

  const entityData = {
    slug: entitySlug,
    ...data,
    content,
  }

  return entityData
}

export function getAllEntities(entityDirectory) {
  const entitiesFiles = getEntityFiles(entityDirectory)
  const allEntities = entitiesFiles.map((entityFile) => {
    return getEntityData(entityFile, entityDirectory)
  })
  return allEntities
}
