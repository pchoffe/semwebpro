import styles from '../../styles/Home.module.css'
import Link from 'next/link'

function Session(props) {
  return (
    <>
      <div className={styles.programsections}>
        {props && (
          <section>
            <div className={styles.sessiontitle}>
              <h3>{props.title}</h3>
            </div>
            {props.session.map((s) => (
              <>
                <div className={styles.flexcontainer}>
                  <div className={styles.presentationtitle}>
                    <Link href={s.slug}>
                      <a>
                        <span className={styles.flexitem}>{s.title}</span>
                      </a>
                    </Link>
                  </div>
                  <div className={styles.presentationspeaker}>
                    <span className={styles.flexitem}>
                      {s.speakers &&
                        s.speakers.map((s) => {
                          return (
                            <span>
                              {s[0]}
                              <br />
                            </span>
                          )
                        })}
                    </span>
                  </div>
                  <hr></hr>
                </div>
              </>
            ))}
          </section>
        )}
      </div>
    </>
  )
}

export default Session
