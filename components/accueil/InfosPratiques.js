function InfosPratiques() {
  return (
    <>
      <h3>Informations pratiques</h3>
      <p>SemWebPro novembre 2020</p>
      <p>
        Contact: <a href="mailto:contact@semweb.pro">contact@semweb.pro</a> /{' '}
        <a href="https://twitter.com/semwebpro">@semwebpro</a> /{' '}
        <a href="https://twitter.com/search?q=%23semwebpro&src=hashtag_click">
          #semwebpro
        </a>
      </p>
    </>
  )
}

export default InfosPratiques
