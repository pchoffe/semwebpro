import dates from '../../sessions/dates-importantes'
import styles from '../../styles/Home.module.css'

function DatesImportantes() {
  return (
    <>
      <div className={styles.dates}>
        <h2 className={styles.purple}>Dates importantes</h2>
        <ul>
          {dates &&
            dates.map((d) => {
              return (
                <li>
                  {d.date} - {d.event}{' '}
                </li>
              )
            })}
        </ul>
      </div>
    </>
  )
}

export default DatesImportantes
