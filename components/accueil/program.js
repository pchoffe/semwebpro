import Session from './session'
import styles from '../../styles/Home.module.css'

function Program(props) {
  return (
    <>
      <div className={styles.program}>
        <h2 className={styles.purple}>Programme</h2>
        {props.program &&
          props.program.map((s) => {
            return <Session session={s[0]} title={s[1]} />
          })}
      </div>
    </>
  )
}

export default Program
