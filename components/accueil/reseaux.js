import reseaux from '../../sessions/reseaux-sociaux.json'
import styles from '../../styles/Home.module.css'
import Link from 'next/link'

function Reseaux() {
  return (
    <>
      <div className={styles.infos}>
        <h2 className={styles.purple}>Réseaux sociaux</h2>
        <p>
          N'hésitez pas à commenter et discuter des présentations vidéos
          ci-dessus en utilisant le mot-dièse #semwebpro sur vos réseaux sociaux
          préférés.
        </p>
        <p>Voici les comptes de SemWeb.Pro que vous pouvez suivre:</p>
        <ul>
          {reseaux &&
            reseaux.map((r) => {
              return (
                <li>
                  <Link href={r.link}>
                    <a>{r.name}</a>
                  </Link>
                </li>
              )
            })}
        </ul>
        <p>
          Pour toute demande d'information, envoyez un courrier électronique à{' '}
          <a href="mailto:contact@semweb.pro">contact@semweb.pro</a>.
        </p>
      </div>
    </>
  )
}

export default Reseaux
