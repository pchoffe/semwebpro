import styles from '../../styles/Home.module.css'
import ReactMarkdownWithHtml from 'react-markdown'

function Calls(props) {
  return (
    <>
      <div>
        <h2 className={styles.purple}>
          Appel à communication / Call for proposal
        </h2>
        {props &&
          props.calls.map((c) => (
            <>
              <div className={styles.appel}>
                <h2 className={styles.purple}>{c.title}</h2>
                <ReactMarkdownWithHtml allowDangerousHtml>
                  {c.content}
                </ReactMarkdownWithHtml>
              </div>
            </>
          ))}
      </div>
    </>
  )
}

export default Calls
