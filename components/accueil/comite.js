import comite from '../../sessions/comite.json'

function Comite() {
  return (
    <>
      <h3>Comité de programme</h3>
      <p>En 2020, le comité de programme est constitué de</p>
      <ul>
        {comite &&
          comite.map((c) => {
            return (
              <li key={c.name}>
                {c.name} ({c.company}){' '}
                {c.contact.link && (
                  <a href={c.contact.link}>{c.contact.name}</a>
                )}
              </li>
            )
          })}
      </ul>
    </>
  )
}

export default Comite
