import styles from '../../styles/Home.module.css'
import Link from 'next/link'

function Intro() {
  return (
    <>
      <div className={styles.container}>
        <p className={styles.description}>
          Journée de présentations et de rencontres
          <br />
          dédiées au web sémantique dans le monde professionnel
        </p>

        <p className={styles.description}>Le 9 décembre 2021, en ligne</p>
      </div>
      <p>
        Les techniques et standards du Web sémantique se sont imposés dans de
        nombreux domaines. En facilitant l'interopérabilité au travers du Web,
        ils donnent un nouveau souffle à l'intégration de données hétérogènes et
        à la construction de graphes de connaissances qui servent de fondations
        à des écosystèmes métiers dynamiques. Cette édition de SemWeb.Pro sera
        l'occasion de mettre en avant les synergies et complémentarités entre le
        Web sémantique et les approches connexes issues des bases de graphe
        (Property Graph, GraphQL, etc), qui accélèrent le développement
        d'applications concrètes dans différents secteurs d'activités :
        industrie, médical, culture et bien d'autres.
      </p>

      <p className={styles.left}>
        <strong>Accessible à tout public !</strong>
      </p>

      <p>
        Vu la durée de la crise sanitaire découlant de l'épidémie de covid-19,
        nous ne pourrons pas cette année réunir tout le monde une journée à
        Paris comme nous le faisions jusqu'à maintenant. Cette année,{' '}
        <strong>
          SemWeb.Pro sera donc organisée "chacun derrière son écran" et sera
          gratuite
        </strong>
        .
      </p>

      <p>
        Retrouvez les éditions précédentes :{' '}
        <Link href="https://semweb.pro/conference/semwebpro2011">2011</Link>,{' '}
        <Link href="https://semweb.pro/conference/semwebpro2012">2012</Link>,{' '}
        <Link href="https://semweb.pro/semwebpro-2014.html">2014</Link>,{' '}
        <Link href="https://semweb.pro/semwebpro-2015.html">2015</Link>,{' '}
        <Link href="https://semweb.pro/semwebpro-2016.html">2016</Link>,{' '}
        <Link href="https://semweb.pro/semwebpro-2017.html">2017</Link>,{' '}
        <Link href="https://semweb.pro/semwebpro-2018.html">2018</Link>{' '}
        <Link href="https://semweb.pro/semwebpro-2019.html">2019</Link> et{' '}
        <Link href="https://semweb.pro/semwebpro-2020.html">2020</Link>.
      </p>
    </>
  )
}

export default Intro
