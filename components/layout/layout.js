import MainTitle from './maintitle'
import Footer from './footer'

function Layout(props) {
  return (
    <>
      <div className="allSite">
        <MainTitle />
        <main className="content">{props.children}</main>
        <Footer />
      </div>
    </>
  )
}

export default Layout
