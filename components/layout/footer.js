import styles from '../../styles/Home.module.css'
import Image from 'next/image'

function Footer() {
  return (
    <>
      <footer className={styles.footer}>
        <div>
          <a
            href="https://www.logilab.fr/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src="/images/logo-logilab.png"
              alt="Logo de Logilab"
              className={styles.logo}
              // loading="lazy"
              width={138}
              height={58}
              objectFit="contain"
              layout="intrinsic"
              quality={50}
              placeholder="blur"
              blurDataURL="data:image/jpeg;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAIAAoDASIAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAb/xAAhEAACAQMDBQAAAAAAAAAAAAABAgMABAUGIWEREiMxUf/EABUBAQEAAAAAAAAAAAAAAAAAAAMF/8QAGhEAAgIDAAAAAAAAAAAAAAAAAAECEgMRkf/aAAwDAQACEQMRAD8AltJagyeH0AthI5xdrLcNM91BF5pX2HaH9bcfaSXWGaRmknyJckliyjqTzSlT54b6bk+h0R//2Q=="
            />
          </a>
        </div>
        <p className={styles.center}>
          +33 1 45 32 03 12
          <br />
          104 bd Auguste Blanqui 75013 Paris
          <br />
          <a href="mailto:contact@logilab.fr">contact@logilab.fr</a>
        </p>
      </footer>
    </>
  )
}

export default Footer
