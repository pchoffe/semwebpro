import styles from '../../styles/Home.module.css'
import Link from 'next/link'

function MainTitle() {
  return (
    <>
      <div className={styles.maintitle}>
        <Link href="/">
          <a className={styles.nounderline}>
            <h1 className={styles.title}>
              <span className={styles.orange}>Sem</span>
              <span className={styles.green}>Web</span>.
              <span className={styles.purple}>Pro</span>
            </h1>
          </a>
        </Link>
      </div>
    </>
  )
}

export default MainTitle
