---
title: 'Call for proposal'
lang: 'en'
---

SemWeb.Pro 2021 - one day for the Semantic Web professionals

**On-line** conference on December 9th, Paris Time.

Organized by Logilab, SemWeb.Pro is a one-day conference focused on the Semantic Web, which has been gathering between 100 to 150 persons each year since its first edition in 2011. Attending SemWeb.Pro is a unique occasion to discuss with members of the Semantic Web community and with innovative companies and industrials implementors of the Web of Linked Data and Knowledge Graphs.

If you have a **successful project** you would like to present, we invite you to send your proposal to share your experience and know-how.

We welcome proposals concerning one of the following **topics**, or any other topic that will get the attention of the program committee: education, gallery-library-archive-museum (GLAM), tourism, physics, industry, environment, buildings, social sciences, theatre, encyclopedia, life sciences, progress of web technology, etc.

The main language is French, but English talks are welcome and if need be, some workshops may happen in english.

### Submission procedure

To submit your talk or use case proposal to the program committee, please send an email to contact@semweb.pro before November 5th, 2021 including the following information :

- title,
- description in less than 400 words,
- bio of the author in a few sentences,
- links to demos, videos, web applications, etc.

### Selection criteria

- actual use of some of the Semantic Web standards is mandatory,
- we favor projects that are already at the production stage or that open new application domains
- demonstrations and videos are valued.

By submitting a proposal, you commit, if it is accepted, to sign an agreement to record your video presentation and publish it freely on the semweb.pro website during the conference.
