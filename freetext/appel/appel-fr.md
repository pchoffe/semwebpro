---
title: 'Appel à communication'
lang: 'fr'
---

SemWeb.Pro 2021 - journée des professionnels du Web Sémantique

Conférence **en ligne** le 9 décembre 2021.

Organisée par Logilab, SemWeb.Pro est une journée de conférence dédiée au Web Sémantique, qui réunit chaque année 100 à 150 personnes depuis la première édition en 2011. Participer à SemWeb.Pro c'est l'occasion d'échanger avec les membres de la communauté du Web Sémantique, ainsi qu'avec des sociétés innovantes et des organisations publiques ou privées qui mettent en œuvre les nouvelles techniques du Web des données et déploient des graphes de connaissances.

Si vous avez une **réalisation** à présenter, nous vous invitons à soumettre dès à présent vos propositions de présentation afin de partager votre savoir-faire et votre expérience.

Nous espérons des propositions abordant l'un des **thèmes** suivants, ou un autre thème qui saura retenir l'attention du comité de programme: éducation, archives-bibliothèques-musées-patrimoine (GLAM), tourisme, sciences physiques, industrie, environnement, bâtiment, sciences sociales, théâtre, encyclopédie, sciences de la vie, avancées techniques concernant le web sémantique, etc.

La langue principale est le français, mais les propositions en anglais sont acceptées et certains ateliers pourront éventuellement avoir lieu en anglais.

### Procédure de soumission

Pour soumettre au comité de programme votre proposition de présentation d'une réalisation ou d'une problématique, veuillez envoyer un courrier électronique à contact@semweb.pro avant le 5 novembre 2021 en précisant les informations suivantes :

- titre,
- description en moins de 400 mots,
- auteur présenté en quelques phrases,
- liens éventuels vers des démos, vidéos, applications, etc.

### Critères de sélection

- l'utilisation effective des standards du Web Sémantique est attendue,
- nous privilégierons les présentations de projets qui sont déjà en production ou qui concernent de nouveaux domaines d'application,
- les démonstrations et vidéos seront appréciées.

En soumettant une proposition, vous vous engagez, si elle est retenue, à signer un accord pour que votre présentation soit enregistrée en vidéo et diffusée librement pendant la durée de la conférence.
