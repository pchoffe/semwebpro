---
title: 'Actualités de la sémantisation des métadonnées archivistiques : le cas des Archives nationales'
session_title: 'education'
about:
  [
    'https://www.ica.org/standards/RiC/ontology',
    'https://github.com/ICA-EGAD/RiC-O',
    'https://www.ica.org/en/about-egad',
  ]
speakers:
  [
    ['Florence Clavaud', 'http://viaf.org/viaf/173490053', '/'],
    [
      'Thomas Francart',
      'http://viaf.org/viaf/106157340583609920929',
      '/personnes/thomas-francart',
    ],
  ]
excerpt: 'Présentation des travaux en cours aux Archives nationales pour mettre en œuvre RiC, le nouveau standard pour la description des archives.'
---

Depuis 2013, un groupe de travail du Conseil international des Archives (ICA-Expert Group on Archival Description ; voir https://www.ica.org/en/about-egad) prépare un nouveau standard pour la description des archives, Records in Contexts (RiC). L’objectif est de fournir au métier des archives un cadre conceptuel abstrait et un modèle formel (ontologie OWL) génériques, centrés sur les entités à décrire et compatibles avec les modèles de métadonnées antérieurs. Comme CIDOC-CRM pour les œuvres d’art et IFLA-LRM pour les livres et autres objets édités conservés en bibliothèque, RiC modélise sous forme de graphe les archives et les différents contextes dans lesquelles celles-ci s’inscrivent. Après la publication d’une version 0.1 du modèle conceptuel RiC-CM en août 2016, le groupe EGAD a publié en décembre 2019 une version 0.2 preview du modèle conceptuel (https://www.ica.org/sites/default/files/ric-cm-0.2_preview.pdf), et une version 0.1 de l’ontologie (https://www.ica.org/standards/RiC/ontology). Les sources de RiC-O, des exemples et pages de documentation ont par ailleurs été rendus publiques sur un entrepôt GitHub en mars 2020 (https://github.com/ICA-EGAD/RiC-O). Les Archives nationales sont très impliquées dans l’élaboration de RiC.

Nous présenterons les travaux en cours aux Archives nationales pour mettre en œuvre RiC. En effet, après la mise en ligne en mars 2018 d’une preuve de concept (PIAAF), des opérations de plus grande ampleur y sont menées.

En particulier, la société Sparna a réalisé pour les Archives nationales RiC-O Converter, un outil de conversion en données RDF conformes à RiC-O de l’ensemble des métadonnées de description des archives conservées dans cette institution. Le code source et la documentation de ce convertisseur ont été publiés sous licence libre en avril 2020 (https://github.com/ArchivesNationalesFR/rico-converter). Nous pourrons présenter rapidement cet outil ainsi que les données RDF des Archives nationales.
