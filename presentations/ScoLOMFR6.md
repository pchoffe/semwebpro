---
title: 'La version 6 de ScoLOMFR et son ontologie'
session_title: 'education'
about:
  ['https://www.reseau-canope.fr/scolomfr/', 'https://www.reseau-canope.fr']
speakers:
  [
    ['Catherine Douvier', 'https://www.idref.fr/069786690', '/'],
    ['Marie Muller', '', '/'],
    [
      'Thomas Francart',
      'http://viaf.org/viaf/106157340583609920929',
      '/personnes/thomas-francart',
    ],
  ]
excerpt: "Présentation de ScolOMFR, standard de description documentaire permettant la mutualisation de ressources numériques pour l'enseignement scolaire"
---

ScoLOMFR est le standard de description documentaire qui permet la mutualisation des ressources numériques, gratuites et payantes pour l'enseignement scolaire. Issu d'un schéma de données reposant sur la norme LOMFR (2010), il est composé d’une soixantaine de champs organisés en 9 éléments et d'un ensemble de vocabulaires communs aux différents acteurs de l'enseignement scolaire.

Créé et maintenu par la DNÉ et Réseau Canopé, ce projet est géré en co-pilotage stratégique entre les deux acteurs. Le pilotage opérationnel est assuré par Réseau Canopé grâce au travail combiné d’une équipe, de terminologues et d’informaticiens, et d’un prestataire expert Thomas Francart (Sparna).

Le Ministère de l’Éducation Nationale, de la Jeunesse et des Sports préconise ScoLOMFR auprès des producteurs de ressources numériques scolaires, pour assurer l’interopérabilité entre leurs données. Sejer, KNÉ, mais aussi le GAR, les projets « Banques de ressources numériques », « EDUbases », « Eduthèque » ou encore « Etincel », pour ne citer qu’eux, l’utilisent. Le nouveau catalogue documentaire de Réseau Canopé, Syrtis, l’intègre également.

Les vocabulaires de ScoLOMFR reprennent le plus fidèlement possible le contenu des programmes et intègrent les dernières réformes engagées par l’éducation nationale.

Cette année, la publication de sa version 6 s’accompagne de l’ontologie ScoLOMFR. Cette dernière permet de déclarer les classes d’objets, les propriétés et les relations qui définissent et lient les concepts entre eux. Ainsi présentée, l’organisation des données et des éléments de ScoLOMFR offre un ensemble structuré dans lequel chaque concept est un élément caractérisé et identifiable au sein d’un tout. Il devient alors possible d’en déduire plus simplement des liens ou des relations plus spécifiques à une ou des recherches ou besoins particuliers.

ScoLOMFR est disponible sur le site : [reseau-canope](https://www.reseau-canope.fr/scolomfr/).
