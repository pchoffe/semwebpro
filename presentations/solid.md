---
title: "Solid, mode d'emploi (donnée non incluses)"
session_title: 'solid'
about: ['https://solidproject.org/']
speakers: [['Nicolas Seydoux', '', '/personnes/nicolas-seydoux']]
excerpt: "Solid est un ensemble de standards visant à amener un nouveau paradigme Web: les données, au lieu d'être accumulées par les applications, sont en la possession de l'utilisateur"
---

Solid est un ensemble de standards visant à amener un nouveau paradigme Web: les données, au lieu d'être accumulées par les applications, sont en la possession de l'utilisateur. Ce principe fondateur a plusieurs conséquences, mais d'un point de vue de développeur, la barre à passer pour construire une application descend d'un cran. Plus besoin de collecter les données: c'est l'utilisateur-ice qui les amène avec lui-elle. Qui mieux que la personne concernée peut déterminer quelles données elle souhaite partager avec vous ? De plus, même sans parler d'infrastructure, les données coûtent cher: en plus de les collecter en premier lieu, il sera de votre responsabilité de les conserver en sécurité, il faudra faire attention à les sauvegarder, tout en se conformant aux règlements comme le RGPD... Pourquoi ne pas couper la poire en deux, pour que des profesionnels puissent se consacrer soit au stockage des données, soit à leur mise en jeu dans des applications, sans être obligé de faire les deux ?

L'architecture Solid repose sur trois composants principaux, qui sont trois services entièrement décentralisés (possiblement auto-hébergés): identité, stockage, et application. Cette présentation donnera une vue d'ensemble de ces trois composants, de leurs interactions, et du rôle de chacun envers l'utilisateur. Pour permettre de mettre ces principes en application, des outils disponibles pour soutenir le développement d'applications sans être expert dans les standards mis en jeux seront aussi présentés.

Évidemment, si un seul standard pouvait régler tous les problèmes du Web et du Web sémantique, ça se saurait. Un stockage de données complètement générique sur lequel les applications viennent se greffer pose évidemment d'épineux problèmes d'interopérabilité, et c'est par l'émergence de bonnes pratiques à travers toute l'industrie que ceux-ci pourront être abordés.

- Une librarie pour interagir avec un Pod Solid: https://github.com/inrupt/solid-client-js
- Deux serveurs Solid open source:
  - https://github.com/solid/community-server,
  - https://github.com/solid/node-solid-server
- Le site web du projet Solid: https://solidproject.org/
