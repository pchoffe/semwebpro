---
title: "SparqlExplorer, utilisation de vues génériques pour l'exploration d'un entrepôt SPARQL"
session_title: 'outils'
about:
  ['https://forge.extranet.logilab.fr/open-source/LDBrowser/sparqlexplorer']
speakers: [['Fabien Amarger', '', '/personnes/fabien-amarger']]
excerpt: "SparqlExplorer, un outil qui permet de naviguer dans les données de n'importe quel entrepôt SPARQL et de les afficher de manière contextualisée."
---

De plus en plus de jeux de données en RDF sont disponibles sur le Web dans des entrepôts SPARQL. Bien que certains logiciels permettent une visualisation de ces données, ils sont souvent soit très spécifiques à un jeu de données particulier, soit trop génériques pour être adaptés à un besoin. Il n'est donc pas toujours aisé d'explorer un jeu de données sans avoir à écrire de requête SPARQL. Ce qui ne rend pas les données accessibles à tous.

Nous proposons SparqlExplorer, un outil qui permet de naviguer dans les données de n'importe quel entrepôt SPARQL et de les afficher de manière contextualisée. En plus de l'URL de l'entrepôt SPARQL à explorer, il est possible de spécifier un serveur de vue, tel que défini dans nos précédents travaux concernant le navigateur pour le web de données liées.

Un serveur de vue met des vues, i.e. composants d'affichage de triplets RDF, à disposition sur le Web. Les vues sont spécifiques à des données RDF. Par exemple, une vue "Carte" permettra la représentation de triplets contenant des informations de géolocalisation. Un mode automatique permet de sélectionner automatiquement la vue du serveur la plus adaptée aux données. Si aucun serveur de vue n'est spécifié, ou aucune vue n'est selectionnée ou applicable pour les triplets RDF à afficher, SparqlExplorer affiche les triplets sous forme tabulaire.

Nous souhaitons poursuivre ces travaux pour permettre de spécifier plusieurs serveurs de vues avec un ordre de priorité. De plus, il serait intéressant d'étendre la notion de vue pour qu'une vue puisse être utilisée dans une autre. De cette façon il serait possible de faire des compositions de vues.

- Dépôt de code libre: https://forge.extranet.logilab.fr/open-source/LDBrowser/sparqlexplorer
- Démonstration en ligne: https://open-source.pages.logilab.fr/LDBrowser/sparqlexplorer/
- Serveur de vues de démonstration: https://culture.pages.logilab.fr/culture-views/
- Sparql endpoint de démonstration: https://sparql-culture.demo.logilab.fr/graphe-culture/sparql
