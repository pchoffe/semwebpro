---
name: 'Nicolas Seydoux'
lastName: 'Seydoux'
viaf: ''
idref: ''
idHAL: 'nicolas-seydoux'
isni: ''
imagePath: '/images/Nicolas-Seydoux-298x300.jpeg'
contributions_hal: 'https://hal.archives-ouvertes.fr/search/index/?qa%5BauthFullName_t%5D%5B%5D=nicolas%20seydoux'
excerpt: "Je suis Nicolas Seydoux, développeur pour Inrupt, une start-up très impliquée dans l'écosystème Solid. J'ai un fort passif autour des technologies du Web Sémantique, notamment appliquées à l'Internet des Objets. J'ai rejoint Inrupt il y a maintenant un peu plus d'un an, ce qui m'a permis de devenir très familier avec les standards impliqués dans Solid. Je fais partie de l'équipe SDK, où nous développons des outils pour faciliter la création d'applications, à destination des développeurs n'étant pas familiers avec les technologies du Web Sémantique."
---
