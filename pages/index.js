import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Intro from '../components/accueil/intro'
import Program from '../components/accueil/program'
import Calls from '../components/accueil/calls'
import Comite from '../components/accueil/comite'
import InfosPratiques from '../components/accueil/InfosPratiques'
import DatesImportantes from '../components/accueil/DatesImportantes'
import Reseaux from '../components/accueil/reseaux'
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'

function HomePage(props) {
  const { education, solid, outils, calls } = props
  const description =
    'semwebpro, une journée de présentations et de rencontres dédiées au web sémantique dans le monde professionnel'

  const program = [
    [education, 'Session Education Droit et Culture du 16 au 22 novembre 2020'],
    [solid, 'Session SOLID du 16 au 22 novembre 2020'],
    [
      outils,
      'Session boîte à outils du Web sémantique du 23 au 29 novembre 2020',
    ],
  ]

  return (
    <div>
      <Head>
        <title>semwebpro</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta property="og:title" content={description} key="title" />
        <meta name="description" content={description} />
      </Head>

      <main className={styles.main}>
        <Intro />
        <Calls calls={calls} />
        <Program program={program} />
        <Reseaux />
        <DatesImportantes />

        <h2 className={styles.purple}>Informations générales</h2>
        <Comite />
        <InfosPratiques />
      </main>
    </div>
  )
}

export async function getStaticProps() {
  const presentations = fs.readdirSync('presentations')

  let prez = presentations.map((file) => {
    const data = fs.readFileSync(`presentations/${file}`).toString()

    return {
      ...matter(data).data,
      slug: 'presentations/' + file.split('.')[0],
    }
  })

  const education = prez.filter((s) => s.session_title == 'education')
  const solid = prez.filter((s) => s.session_title == 'solid')
  const outils = prez.filter((s) => s.session_title == 'outils')

  function getEntityFiles(entityDirectory) {
    return fs.readdirSync(entityDirectory)
  }
  function getEntityData(entityIdentifier, entityDirectory) {
    const entitySlug = entityIdentifier.replace(/\.md$/, '') // removes the file extension
    const filePath = path.join(entityDirectory, `${entitySlug}.md`)
    const fileContent = fs.readFileSync(filePath, 'utf-8')
    const { data, content } = matter(fileContent)

    const entityData = {
      slug: entitySlug,
      ...data,
      content,
    }

    return entityData
  }
  function getAllEntities(entityDirectory) {
    const entitiesFiles = getEntityFiles(entityDirectory)
    const allEntities = entitiesFiles.map((entityFile) => {
      return getEntityData(entityFile, entityDirectory)
    })
    return allEntities
  }

  const calls = getAllEntities('freetext/appel')

  return {
    props: {
      education,
      solid,
      outils,
      calls,
    },
  }
}

export default HomePage
