import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import ReactMarkdownWithHtml from 'react-markdown'
import matter from 'gray-matter'
import styles from '../../styles/Home.module.css'
import fs from 'fs'
import path from 'path'

function PersonPage(props) {
  const { person, presentations } = props
  const description = person.excerpt
  const title = person.name

  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta property="og:title" content={title} key="title" />
        <meta name="description" content={description} />
      </Head>
      <main className={styles.toppages}>
        <Image
          src={person.imagePath}
          alt={`Portrait de ${title}`}
          loading="lazy"
          width={250}
          height={250}
          objectFit="contain"
          layout="intrinsic"
          quality={50}
          placeholder="blur"
          blurDataURL="data:image/jpeg;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAIAAoDASIAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAb/xAAhEAACAQMDBQAAAAAAAAAAAAABAgMABAUGIWEREiMxUf/EABUBAQEAAAAAAAAAAAAAAAAAAAMF/8QAGhEAAgIDAAAAAAAAAAAAAAAAAAECEgMRkf/aAAwDAQACEQMRAD8AltJagyeH0AthI5xdrLcNM91BF5pX2HaH9bcfaSXWGaRmknyJckliyjqTzSlT54b6bk+h0R//2Q=="
        />
        <h1>{person.name}</h1>
        <ReactMarkdownWithHtml allowDangerousHtml>
          {description}
        </ReactMarkdownWithHtml>
        <p>Présentations de {person.name} cette année</p>
        <ul>
          {presentations.map((p) => (
            <li>
              <Link href={p.prezslug}>{p.preztitle}</Link>
            </li>
          ))}
        </ul>
      </main>
    </>
  )
}

export function getStaticProps(context) {
  const { params } = context
  const { slug } = params

  function getEntityData(entityIdentifier, entityDirectory) {
    const entitySlug = entityIdentifier.replace(/\.md$/, '') // removes the file extension
    const filePath = path.join(entityDirectory, `${entitySlug}.md`)
    const fileContent = fs.readFileSync(filePath, 'utf-8')
    const { data, content } = matter(fileContent)
    const entityData = {
      slug: entitySlug,
      ...data,
      content,
    }
    return entityData
  }

  const person = getEntityData(slug, 'personnes')

  function getEntityFiles(entityDirectory) {
    return fs.readdirSync(entityDirectory)
  }

  function getAllEntities(entityDirectory) {
    const entitiesFiles = getEntityFiles(entityDirectory)
    const allEntities = entitiesFiles.map((entityFile) => {
      return getEntityData(entityFile, entityDirectory)
    })
    return allEntities
  }
  const prez = getAllEntities('presentations')
  const prezspeakers = prez.map((p) => {
    return {
      prezslug: '/presentations/' + p.slug,
      preztitle: p.title,
      prezspeakers: p.speakers,
    }
  })

  const presentations = prezspeakers.filter((obj) => {
    obj.prezspeakers = obj.prezspeakers.filter(
      (el) => el[2] == '/personnes/' + slug
    )
    return obj.prezspeakers.length > 0
  })

  return {
    props: {
      person,
      slug,
      presentations,
    },
  }
}

export function getStaticPaths() {
  function getEntityFiles(entityDirectory) {
    return fs.readdirSync(entityDirectory)
  }
  const personsFilenames = getEntityFiles('personnes')
  const slugs = personsFilenames.map((fileName) =>
    fileName.replace(/\.md$/, '')
  )

  return {
    paths: slugs.map((slug) => ({ params: { slug: slug } })),
    fallback: false,
  }
}

export default PersonPage
