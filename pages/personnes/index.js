import classes from '../../styles/Home.module.css'
import Head from 'next/head'
import Link from 'next/link'
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'

function AllPersonsPage(props) {
  const { persons } = props
  const title = 'Les auteurs'

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={title} />
        <meta property="og:title" content={title} key="title" />
      </Head>
      <div>
        <h1>Tous les auteurs</h1>
        <p>Une liste d&apos;auteurs, triée par ordre alphabétique.</p>

        <ul>
          {persons
            .sort((a, b) => (a.lastName > b.lastName ? 1 : -1))
            .map((p) => (
              <>
                <li>
                  <Link href={`/personnes/${p.slug}`} passHref>
                    {p.name}
                  </Link>
                </li>
              </>
            ))}
        </ul>
      </div>
    </>
  )
}

export function getStaticProps() {
  function getEntityFiles(entityDirectory) {
    return fs.readdirSync(entityDirectory)
  }

  function getAllEntities(entityDirectory) {
    const entitiesFiles = getEntityFiles(entityDirectory)
    const allEntities = entitiesFiles.map((entityFile) => {
      return getEntityData(entityFile, entityDirectory)
    })
    return allEntities
  }

  function getEntityData(entityIdentifier, entityDirectory) {
    const entitySlug = entityIdentifier.replace(/\.md$/, '') // removes the file extension
    const filePath = path.join(entityDirectory, `${entitySlug}.md`)
    const fileContent = fs.readFileSync(filePath, 'utf-8')
    const { data, content } = matter(fileContent)

    const entityData = {
      slug: entitySlug,
      ...data,
      content,
    }

    return entityData
  }

  const persons = getAllEntities('personnes')

  return {
    props: {
      persons,
    },
  }
}

export default AllPersonsPage
