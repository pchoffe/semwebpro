import Head from 'next/head'
import Link from 'next/link'
import ReactMarkdownWithHtml from 'react-markdown'
import matter from 'gray-matter'
import fs from 'fs'
import path from 'path'

function PresentationPage(props) {
  const { presentation } = props
  const description = presentation.excerpt
  const speakers = presentation.speakers
  const title = presentation.title
  const performer = speakers.map((s) => ({
    '@type': 'Person',
    name: s[0],
    identifier: s[1],
  }))
  const about = presentation.about

  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta property="og:title" content={title} key="title" />
        <meta name="description" content={description} />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              '@context': 'https://schema.org/',
              '@type': 'Event',
              title,
              startDate: '2020-07-24T16:00Z',
              endDate: '2020-07-24T16:30Z',
              eventAttendanceMode:
                'https://schema.org/OnlineEventAttendanceMode',
              eventStatus: 'https://schema.org/EventScheduled',
              location: {
                '@type': 'VirtualLocation',
                url: 'https://stream.storytimereadings.com/',
              },
              description,
              url: 'https://www.semweb.pro/semwebpro-2020.html',
              organizer: {
                '@type': 'Organization',
                name: 'Logilab',
                url: 'https://www.logilab.fr/',
              },
              performer,
              about,
            }),
          }}
        />
      </Head>
      <h1>{presentation.title}</h1>
      <p>
        Présenté par :{' '}
        {speakers.map((s) => {
          return (
            <span>
              <Link href={s[2]}>{s[0]}</Link>&nbsp;&nbsp;
            </span>
          )
        })}{' '}
      </p>
      <ReactMarkdownWithHtml allowDangerousHtml>
        {presentation.content}
      </ReactMarkdownWithHtml>
    </>
  )
}

export function getStaticProps(context) {
  const { params } = context
  const { slug } = params

  function getEntityData(entityIdentifier, entityDirectory) {
    const entitySlug = entityIdentifier.replace(/\.md$/, '') // removes the file extension
    const filePath = path.join(entityDirectory, `${entitySlug}.md`)
    const fileContent = fs.readFileSync(filePath, 'utf-8')
    const { data, content } = matter(fileContent)

    const entityData = {
      slug: entitySlug,
      ...data,
      content,
    }
    return entityData
  }

  const presentationData = getEntityData(slug, 'presentations')

  return {
    props: {
      presentation: presentationData,
      slug,
    },
  }
}

export function getStaticPaths() {
  function getEntityFiles(entityDirectory) {
    return fs.readdirSync(entityDirectory)
  }
  const presentationsDirectory = path.join(process.cwd(), 'presentations')
  const presentationsFilenames = getEntityFiles(presentationsDirectory)
  const slugs = presentationsFilenames.map((fileName) =>
    fileName.replace(/\.md$/, '')
  )

  return {
    paths: slugs.map((slug) => ({ params: { slug: slug } })),
    fallback: false,
  }
}

export default PresentationPage
