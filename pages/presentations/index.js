import classes from '../../styles/Home.module.css'
import Head from 'next/head'
import Link from 'next/link'
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'

function AllPresentationsPage(props) {
  const { presentations } = props
  const title = 'Les présentations'

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={title} />
        <meta property="og:title" content={title} key="title" />
      </Head>
      <div>
        <h1>Toutes les présentations</h1>
        <p>La liste des présentations</p>

        <ul>
          {presentations.map((p) => (
            <>
              <li>
                <Link href={`/presentations/${p.slug}`} passHref>
                  {p.title}
                </Link>
              </li>
            </>
          ))}
        </ul>
      </div>
    </>
  )
}

export function getStaticProps() {
  function getEntityFiles(entityDirectory) {
    return fs.readdirSync(entityDirectory)
  }

  function getAllEntities(entityDirectory) {
    const entitiesFiles = getEntityFiles(entityDirectory)
    const allEntities = entitiesFiles.map((entityFile) => {
      return getEntityData(entityFile, entityDirectory)
    })
    return allEntities
  }

  function getEntityData(entityIdentifier, entityDirectory) {
    const entitySlug = entityIdentifier.replace(/\.md$/, '') // removes the file extension
    const filePath = path.join(entityDirectory, `${entitySlug}.md`)
    const fileContent = fs.readFileSync(filePath, 'utf-8')
    const { data, content } = matter(fileContent)

    const entityData = {
      slug: entitySlug,
      ...data,
      content,
    }

    return entityData
  }

  const allPresentations = getAllEntities('presentations')

  return {
    props: {
      presentations: allPresentations,
    },
  }
}

export default AllPresentationsPage
